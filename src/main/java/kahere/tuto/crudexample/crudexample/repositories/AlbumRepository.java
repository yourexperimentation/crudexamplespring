package kahere.tuto.crudexample.crudexample.repositories;

import kahere.tuto.crudexample.crudexample.bussiness.Album;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AlbumRepository extends JpaRepository<Album, Long> {

}
