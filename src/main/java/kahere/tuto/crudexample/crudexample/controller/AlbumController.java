package kahere.tuto.crudexample.crudexample.controller;

import kahere.tuto.crudexample.crudexample.bussiness.Album;
import kahere.tuto.crudexample.crudexample.repositories.AlbumRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/albums")
public class AlbumController {
    private final AlbumRepository albumRepository;

    @Autowired
    public AlbumController(AlbumRepository albumRepository) {
        this.albumRepository = albumRepository;
    }

    @PostMapping
    public ResponseEntity<Album> createAlbum(@RequestBody Album album){
       Album album1 = this.albumRepository.save(album);
       return new ResponseEntity<>(album1, HttpStatus.CREATED);
    }
    @GetMapping("/")
    public ResponseEntity<Collection<Album>> getAllAlbums(){
        List<Album> l = this.albumRepository.findAll();
        return new ResponseEntity<>(l, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Album> getOneAlbum(@PathVariable("id") Long id){
        Optional<Album> optionalAlbum = this.albumRepository.findById(id);
        return optionalAlbum.map(album -> new ResponseEntity<>(album, HttpStatus.OK)).orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @PutMapping("/{id}")
    public ResponseEntity<Album> updateAlbum(@PathVariable("id") Long id, @RequestBody Album album){
        Album album1 = this.albumRepository.findById(id).get();
        album1.setAuthor(album.getAuthor());
        album1.setNumberOfTitle(album.getNumberOfTitle());
        album1.setTitle(album.getTitle());
        return new ResponseEntity<>(album1, HttpStatus.OK);
    }
}
