package kahere.tuto.crudexample.crudexample.bussiness;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "album")
@Getter
@Setter
@NoArgsConstructor
public class Album {
    @Id
    @GeneratedValue
    private Long id;
    private String title;
    private int numberOfTitle;
    private String author;

}
